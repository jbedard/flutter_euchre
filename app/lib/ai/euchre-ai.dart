import 'package:app/cards/playing-card.dart';
import 'package:app/game/call-result.dart';
import 'package:app/game/game-state.dart';

abstract class EuchreAI {
  Future<bool> desiresBid(GameState state);
  Future<CallResult> maybeCallSuit(GameState state);
  Future<PlayingCard> chooseDiscard(GameState state);
  Future<PlayingCard> play(GameState state);
}