import 'dart:math';

import 'package:app/ai/euchre-ai.dart';
import 'package:app/cards/playing-card.dart';
import 'package:app/cards/suit.dart';
import 'package:app/cards/card-util.dart';
import 'package:app/game/call-result.dart';
import 'package:app/game/game-state.dart';

class DerpAI implements EuchreAI {
  final Random r = Random();

  @override
  Future<bool> desiresBid(GameState state) {
    return Future.delayed(Duration(milliseconds: 200 + r.nextInt(500)), () => false);
  }

  @override
  Future<CallResult> maybeCallSuit(GameState state) {
    return Future.delayed(Duration(milliseconds: 200 + r.nextInt(500)), () => CallResult(false, Suit.values[r.nextInt(Suit.values.length)]));
  }

  @override
  Future<PlayingCard> chooseDiscard(GameState state) {
    List<PlayingCard> myHand = state.players[state.dealerPosition];
    return Future.delayed(Duration(milliseconds: 200 + r.nextInt(500)), () => myHand[r.nextInt(myHand.length)]);
  }

  @override
  Future<PlayingCard> play(GameState state) {
    List<PlayingCard> myHand = state.players[state.currentPlayer];
    List<PlayingCard> validPlays = state.currentTrick.empty
      ? List<PlayingCard>.from(myHand)
      : getValidPlays(myHand, state.currentTrick, state.trump);
    return Future.delayed(Duration(milliseconds: 200 + r.nextInt(500)), () => validPlays[r.nextInt(validPlays.length)]);
  }
}