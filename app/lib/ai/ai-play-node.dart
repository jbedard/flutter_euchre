import 'package:app/cards/playing-card.dart';
import 'package:app/game/game-state.dart';
import 'package:app/game/table-position.dart';
import 'package:app/game/team.dart';

import 'ai-utilities.dart';

class AIPlayNode {
  final PlayingCard play;
  final GameState state;
  final List<AIPlayNode> children;

  AIPlayNode(this.play, this.state, this.children);

  int getScore() {
    if (this.children.length > 0) {
      Team team = state.currentPlayer.team;
      int bestScore = team == Team.north_south ? -999 : 999;
      for (AIPlayNode c in children) {
        int score = c.getScore();
        if (team == Team.north_south) {
          if (score > bestScore) {
            bestScore = score;
          }
        } else {
          if (score < bestScore) {
            bestScore = score;
          }
        }
      }
      return bestScore;
    } else {
      return state.pastTricks[Team.north_south].length - state.pastTricks[Team.east_west].length;
    }
  }

  int get size {
    int totalSize = 0;
    for (AIPlayNode c in children) {
      totalSize += c.size;
    }
    return 1 + totalSize;
  }

  void deepen() {
    if (this.state.totalTricks == 5) {
      return;
    }

    if (this.children.length == 0) {
      List<PlayingCard> plays = getGoodPlays(state);
      for (PlayingCard c in plays) {
        GameState newState = state.handlePlay(c);
        if (newState.currentTrick.finished) {
          newState = newState.newTrick();
          AIPlayNode newNode = AIPlayNode(c, newState, []);
          children.add(newNode);
        } else {
          AIPlayNode newNode = AIPlayNode(c, newState, []);
          children.add(newNode);
          newNode.deepen();
        }
      }
    } else {
      for (AIPlayNode n in children) {
        n.deepen();
      }
    }
  }

  void trim() {
    Team myTeam = state.currentPlayer.team;
    List<AIPlayNode> nonLosers = [];
    List<AIPlayNode> losers = [];
    for (AIPlayNode n in children) {
      int score = n.getScore();
      if (myTeam == Team.north_south) {
        if (score > -3) {
          nonLosers.add(n);
        } else {
          losers.add(n);
        }
      } else {
        if (score < 3) {
          nonLosers.add(n);
        } else {
          losers.add(n);
        }
      }
    }
    if (!nonLosers.isEmpty && nonLosers.length != children.length) {
      children.clear();
      children.addAll(nonLosers);
    }
    for (AIPlayNode n in children) {
      n.trim();
    }
  }
}