import 'package:app/ai/euchre-ai.dart';
import 'package:app/cards/playing-card.dart';
import 'package:app/game/call-result.dart';
import 'package:app/game/game-state.dart';

class GoodAI implements EuchreAI {
  @override
  Future<PlayingCard> chooseDiscard(GameState state) {
    // TODO: implement chooseDiscard
    return null;
  }

  @override
  Future<bool> desiresBid(GameState state) {
    // TODO: implement desiresBid
    return null;
  }

  @override
  Future<CallResult> maybeCallSuit(GameState state) {
    // TODO: implement maybeCallSuit
    return null;
  }

  @override
  Future<PlayingCard> play(GameState state) {
    // TODO: implement play
    return null;
  }

}