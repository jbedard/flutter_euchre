
import 'package:app/cards/card-scoring.dart';
import 'package:app/cards/card-util.dart';
import 'package:app/cards/playing-card.dart';
import 'package:app/cards/suit.dart';
import 'package:app/game/game-state.dart';
import 'package:app/game/table-position.dart';
import 'package:app/game/trick.dart';

List<PlayingCard> getGoodPlays(GameState state) {
  List<PlayingCard> validPlays = getValidPlays(state.players[state.currentPlayer], state.currentTrick, state.trump);

  if (state.currentTrick.empty) {
    // maybe do something else here...
    return validPlays;
  }

  var winningPlays = List<PlayingCard>();
  Map<Suit, List<PlayingCard>> losingPlays = {
    Suit.spades: List<PlayingCard>(),
    Suit.hearts: List<PlayingCard>(),
    Suit.diamonds: List<PlayingCard>(),
    Suit.clubs: List<PlayingCard>(),
  };

  for (PlayingCard play in validPlays) {
    Trick withPlay = state.currentTrick.play(state.currentPlayer, play);
    TablePosition newWinner = withPlay.determineWinner();
    if (newWinner == state.currentPlayer) {
      winningPlays.add(play);
    } else {
      losingPlays[play.getSuit(state.trump)].add(play);
    }
  }

  var goodPlays = List<PlayingCard>();
  for (List<PlayingCard> losers in losingPlays.values) {
    if (losers.isNotEmpty) {
      losers.sort((c1, c2) => discardScore(c1, state.trump) - discardScore(c2, state.trump));
      goodPlays.add(losers[0]);
    }
  }
  if (winningPlays.isNotEmpty) {
    winningPlays.sort((c1, c2) => trickCardScore(c1, state.currentTrick.leadSuit, state.currentTrick.trump) - trickCardScore(c1, state.currentTrick.leadSuit, state.currentTrick.trump));
    goodPlays.add(winningPlays[0]);
    int lastScore = trickCardScore(winningPlays[0], state.currentTrick.leadSuit, state.currentTrick.trump);
    for (int i = 1; i < winningPlays.length; i++) {
      int score = trickCardScore(winningPlays[i], state.currentTrick.leadSuit, state.currentTrick.trump);
      if (score > (lastScore + 1)) {
        // these cards aren't sequential
        goodPlays.add(winningPlays[i]);
      }
      lastScore = score;
    }
  }

  return goodPlays;
}

Map<TablePosition, Set<Suit>> getShortSuitsBasedOnPastTricks(GameState gs) {
  Map<TablePosition, Set<Suit>> shortSuits = {
    TablePosition.north: {},
    TablePosition.east: {},
    TablePosition.south: {},
    TablePosition.west: {},
  };
  for (List<Trick> trickList in gs.pastTricks.values) {
    for (Trick trick in trickList) {
      TablePosition curr = trick.lead;
      Suit leadSuit = trick.leadSuit;
      for (int i = 0; i < 3; i++) {
        curr = curr.next;
        if (trick.plays[curr].getSuit(gs.trump) != leadSuit) {
          shortSuits[curr].add(leadSuit);
        }
      }
    }
  }
  return shortSuits;
}

List<PlayingCard> getUnknownCardsForCurrentPlayer(GameState gs) {
  List<PlayingCard> deck = [];
  gs.players.forEach((key, value) {
    if (key != gs.currentPlayer) {
      deck.addAll(value);
    }
  });
  deck.addAll(gs.kitty.buried);
  deck.remove(gs.kitty.upCard);
  return deck;
}

GameState inventPossibleState(GameState gs) {
  Map<TablePosition, Set<Suit>> shortSuits = getShortSuitsBasedOnPastTricks(gs);
  List<PlayingCard> deck = getUnknownCardsForCurrentPlayer(gs);

  Map<TablePosition, List<PlayingCard>> possiblePlayers = Map.fromIterable(TablePosition.values, key: (v) => v, value: (v) => v == gs.currentPlayer ? gs.players[v] : []);
  if (gs.players[gs.dealerPosition].contains(gs.kitty.upCard)) {
    possiblePlayers[gs.dealerPosition].add(gs.kitty.upCard);
  }

  
  return GameState(
    phase: gs.phase,
    dealerPosition: gs.dealerPosition,
    players: possiblePlayers,
    kitty: gs.kitty,
    currentPlayer: gs.currentPlayer,
    bidWinner: gs.bidWinner,
    trump: gs.trump,
    currentTrick: gs.currentTrick,
    pastTricks: gs.pastTricks
  );
}