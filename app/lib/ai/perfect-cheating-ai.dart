import 'dart:math';

import 'package:app/ai/ai-play-node.dart';
import 'package:app/ai/ai-utilities.dart';
import 'package:app/ai/euchre-ai.dart';
import 'package:app/cards/card-scoring.dart';
import 'package:app/cards/card-value.dart';
import 'package:app/cards/playing-card.dart';
import 'package:app/cards/suit.dart';
import 'package:app/game/call-result.dart';
import 'package:app/game/game-state.dart';
import 'package:app/game/table-position.dart';
import 'package:app/game/team.dart';

class PerfectCheatingAI implements EuchreAI {
  final Random r = Random();

  @override
  Future<PlayingCard> chooseDiscard(GameState state) {
    return Future.delayed(Duration(milliseconds: 100), () => _getBestDiscard(state));
  }

  @override
  Future<bool> desiresBid(GameState state) {
    return Future.delayed(Duration(milliseconds: 100), () => _doDesiresBid(state));
  }

  @override
  Future<CallResult> maybeCallSuit(GameState state) {
    return Future.delayed(Duration(milliseconds: 100), () => _getCall(state));
  }

  @override
  Future<PlayingCard> play(GameState state) {
    return Future.delayed(Duration(milliseconds: 100), () => _getBestPlay(state)).then((value) {
      Future<PlayingCard> play2 = _getBestPlay2(state);
      return play2;
    });
  }

  Future<bool> _doDesiresBid(GameState state) {
    print('${state.currentPlayer.name} bidding...');
    Stopwatch stopwatch = Stopwatch()..start();
    Team myTeam = state.currentPlayer.team;
    return chooseDiscard(state).then((card) {
      GameState withDiscard = state.handleDiscard(card);
      int score = _simulateGame(withDiscard);
      print('${state.currentPlayer.name} took ${stopwatch.elapsed} to bid with score ${score}');
      return myTeam == Team.north_south && score > 0 || myTeam == Team.east_west && score < 0;
    });
  }

  PlayingCard _getBestDiscard(GameState state) {
    Suit trump = state.kitty.upCard.baseSuit;
    Map<Suit, List<PlayingCard>> cardsBySuit = {
      Suit.spades: List<PlayingCard>(),
      Suit.hearts: List<PlayingCard>(),
      Suit.diamonds: List<PlayingCard>(),
      Suit.clubs: List<PlayingCard>(),
    };
    List<PlayingCard> hand = List.from(state.players[state.dealerPosition]);
    for (PlayingCard card in hand) {
      cardsBySuit[card.getSuit(trump)].add(card);
    }
    List<PlayingCard> viable = List<PlayingCard>();
    for (Suit suit in [ trump.opposite.sister, trump.opposite, trump.sister ]) {
      if (cardsBySuit[suit].length == 1 && cardsBySuit[suit][0].value != CardValue.ace) {
        viable.add(cardsBySuit[suit][0]);
      }
    }
    if (viable.isNotEmpty) {
      viable.sort((c1, c2) => c1.value.baseValue - c2.value.baseValue);
      return viable[0];
    }
    hand.sort((c1, c2) => discardScore(c1, trump) - discardScore(c2, trump));
    return hand[0];
  }

  CallResult _getCall(GameState state) {
    Team myTeam = state.currentPlayer.team;
    CallResult call = CallResult(false, Suit.spades);
    int bestScore = myTeam == Team.north_south ? -999 : 999;
    for (Suit suit in Suit.values) {
      int score = _simulateGame(state.setTrumpAndPlay(suit));
      if (myTeam == Team.north_south) {
        if (score > 0 && score > bestScore) {
          call = CallResult(true, suit);
        }
      } else {
        if (score < 0 && score < bestScore) {
          call = CallResult(true, suit);
        }
      }
    }
    return call;
  }

  Future<PlayingCard> _getBestPlay2(GameState state) async {
    // print('${state.currentPlayer.name} is thinking with 2nd method...');
    Stopwatch stopwatch = Stopwatch()..start();

    AIPlayNode root = AIPlayNode(null, state, []);
    root.deepen();
    root.deepen();
    root.deepen();
    // print('total size ${root.size}');
    root.trim();
    // print('total size after trimming is ${root.size}');
    root.deepen();
    // print('total size ${root.size}');
    root.trim();
    // print('total size after trimming is ${root.size}');
    root.deepen();
    root.deepen();

    Team myTeam = state.currentPlayer.team;
    int bestScore = myTeam == Team.north_south ? -999 : 999;
    AIPlayNode bestPlay = null;
    for (AIPlayNode nextPossiblePlay in root.children) {
      int score = nextPossiblePlay.getScore();
      if (myTeam == Team.north_south) {
        if (score > bestScore) {
          bestScore = score;
          bestPlay = nextPossiblePlay;
        }
      } else {
        if (score < bestScore) {
          bestScore = score;
          bestPlay = nextPossiblePlay;
        }
      }
    }

    if (bestPlay.getScore() == 4) {
      print('it is 4, wtf');
    }

    print('${state.currentPlayer.name} found best play ${bestPlay.play.toString()} in ${stopwatch.elapsed} with score ${bestScore} using method 2');
    return bestPlay.play;
  }

  PlayingCard _getBestPlay(GameState state) {
    // print('${state.currentPlayer.name} is thinking...');
    Stopwatch stopwatch = Stopwatch()..start();
    Team myTeam = state.currentPlayer.team;
    int bestScore = myTeam == Team.north_south ? -999 : 999;
    PlayingCard bestPlay;
    List<PlayingCard> plays = getGoodPlays(state);
    for (PlayingCard play in plays) {
      GameState newState = state.handlePlay(play);
      int score = _simulateGame(newState);
      if (myTeam == Team.north_south) {
        if (score > bestScore) {
          bestScore = score;
          bestPlay = play;
        }
      } else {
        if (score < bestScore) {
          bestScore = score;
          bestPlay = play;
        }
      }
    }
    print('${state.currentPlayer.name} found best play ${bestPlay.toString()} in ${stopwatch.elapsed} with score ${bestScore} using method 1');
    return bestPlay;
  }

  int _simulateGame(GameState state) {
    if (state.currentTrick.finished) {
      GameState newState = state.newTrick();
      if (newState.totalTricks == 5) {
        return newState.pastTricks[Team.north_south].length - newState.pastTricks[Team.east_west].length;
      }
      return _simulateGame(newState);
    } else {
      Team myTeam = state.currentPlayer.team;
      int bestScore = myTeam == Team.north_south ? -999 : 999;
      List<PlayingCard> plays = getGoodPlays(state);
      for (PlayingCard play in plays) {
        GameState newState = state.handlePlay(play);
        int score = _simulateGame(newState);
        if (myTeam == Team.north_south) {
          if (score > bestScore) {
            bestScore = score;
          }
        } else {
          if (score < bestScore) {
            bestScore = score;
          }
        }
      }
      return bestScore;
    }
  }
}