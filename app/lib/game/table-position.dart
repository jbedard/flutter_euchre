import 'package:app/game/team.dart';

enum TablePosition {
  north, east, south, west
}

extension TablePositionExtension on TablePosition {
  String get name {
    switch (this) {
      case TablePosition.north: return "north";
      case TablePosition.east: return "east";
      case TablePosition.south: return "south";
      case TablePosition.west: return "west";
    }
  }

  Team get team {
    switch (this) {
      case TablePosition.north: return Team.north_south;
      case TablePosition.east: return Team.east_west;
      case TablePosition.south: return Team.north_south;
      case TablePosition.west: return Team.east_west;
    }
  }

  TablePosition get partner {
    switch (this) {
      case TablePosition.north: return TablePosition.south;
      case TablePosition.east: return TablePosition.west;
      case TablePosition.south: return TablePosition.north;
      case TablePosition.west: return TablePosition.west;
    }
  }

  TablePosition get prev {
    switch (this) {
      case TablePosition.north: return TablePosition.west;
      case TablePosition.east: return TablePosition.north;
      case TablePosition.south: return TablePosition.east;
      case TablePosition.west: return TablePosition.south;
    }
  }

  TablePosition get next {
    switch (this) {
      case TablePosition.north: return TablePosition.east;
      case TablePosition.east: return TablePosition.south;
      case TablePosition.south: return TablePosition.west;
      case TablePosition.west: return TablePosition.north;
    }
  }
}