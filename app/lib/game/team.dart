

enum Team {
  north_south,
  east_west
}

extension TeamExtension on Team {
  String get name {
    switch (this) {
      case Team.north_south: return "north south";
      case Team.east_west: return "east west";
    }
  }
}