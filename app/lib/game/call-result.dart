
import 'package:app/cards/suit.dart';

class CallResult {
  final bool call;
  final Suit suit;

  const CallResult(this.call, this.suit);
}

typedef CallSuitCallback = void Function(CallResult result);