import 'package:app/cards/playing-card.dart';

class Kitty {
  final List<PlayingCard> buried;
  final PlayingCard upCard;
  final PlayingCard discardCard;

  const Kitty(this.buried, this.upCard, this.discardCard);
}