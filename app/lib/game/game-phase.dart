enum GamePhase {
  first_bidding_round,
  second_bidding_round,
  discard,
  play,
  resolve_trick,
  finished
}