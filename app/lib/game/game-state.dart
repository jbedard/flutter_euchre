import 'package:app/cards/deck.dart';
import 'package:app/cards/playing-card.dart';
import 'package:app/cards/suit.dart';
import 'package:app/game/table-position.dart';
import 'package:app/game/team.dart';
import 'package:app/game/trick.dart';
import 'game-phase.dart';
import 'kitty.dart';

GameState startNewGame(TablePosition dealer) {
  List<PlayingCard> deck = newShuffledDeck();
  Map<TablePosition, List<PlayingCard>> players = {
    TablePosition.north: List<PlayingCard>(),
    TablePosition.east: List<PlayingCard>(),
    TablePosition.south: List<PlayingCard>(),
    TablePosition.west: List<PlayingCard>(),
  };
  for (int i = 0; i < 5; i++) {
    TablePosition p = dealer.next;
    for (int i = 0; i < 4; i++) {
      players[p].add(deck.removeLast());
      p = p.next;
    }
  }
  Kitty kitty = Kitty([ deck.removeLast(), deck.removeLast(), deck.removeLast()], deck.removeLast(), null);
  return GameState(
    phase: GamePhase.first_bidding_round,
    dealerPosition: dealer,
    players: players,
    kitty: kitty,
    currentPlayer: dealer.next,
    bidWinner: TablePosition.north, // recorded when bid is finished
    trump: Suit.spades, // not used until playing
    currentTrick: Trick(Suit.spades, dealer.next, Map<TablePosition, PlayingCard>()),
    pastTricks: { Team.north_south: List<Trick>(), Team.east_west: List<Trick>() }
  );
}

class GameState {
  final GamePhase phase;
  final TablePosition dealerPosition;
  final Map<TablePosition, List<PlayingCard>> players;
  final Kitty kitty;
  final TablePosition currentPlayer;
  final TablePosition bidWinner;
  final Suit trump;
  final Trick currentTrick;
  final Map<Team, List<Trick>> pastTricks;

  const GameState({
    this.phase,
    this.dealerPosition,
    this.players,
    this.kitty,
    this.currentPlayer,
    this.bidWinner,
    this.trump,
    this.currentTrick,
    this.pastTricks
  });

  GameState nextPlayer() => GameState(
    phase: phase,
    dealerPosition: dealerPosition,
    players: players,
    kitty: kitty,
    currentPlayer: currentPlayer.next,
    bidWinner: bidWinner,
    trump: trump,
    currentTrick: currentTrick,
    pastTricks: pastTricks
  );

  GameState withPhase(GamePhase newPhase) => GameState(
    phase: newPhase,
    dealerPosition: dealerPosition,
    players: players,
    kitty: kitty,
    currentPlayer: dealerPosition.next, // current player resets to left of dealer
    bidWinner: bidWinner,
    trump: trump,
    currentTrick: currentTrick,
    pastTricks: pastTricks
  );

  GameState orderUp() => GameState(
    phase: GamePhase.discard,
    dealerPosition: dealerPosition,
    players: players,
    kitty: kitty,
    currentPlayer: dealerPosition, // dealer is the discarder
    bidWinner: currentPlayer, // current player won the bid
    trump: trump,
    currentTrick: currentTrick,
    pastTricks: pastTricks
  );

  GameState handleDiscard(PlayingCard discardCard) {
    List<PlayingCard> newDealerHand = List<PlayingCard>.from(players[dealerPosition]);
    newDealerHand.remove(discardCard);
    newDealerHand.add(kitty.upCard);
    Map<TablePosition, List<PlayingCard>> newPlayers = players.map((pos, hand) => MapEntry<TablePosition, List<PlayingCard>>(pos, pos == dealerPosition ? newDealerHand : hand));
    return GameState(
      phase: GamePhase.play,
      dealerPosition: dealerPosition,
      players: newPlayers,
      kitty: Kitty(kitty.buried, kitty.upCard, discardCard),
      currentPlayer: dealerPosition.next,
      bidWinner: currentPlayer,
      trump: kitty.upCard.baseSuit,
      currentTrick: Trick(kitty.upCard.baseSuit, dealerPosition.next, Map<TablePosition, PlayingCard>()),
      pastTricks: { Team.north_south: List<Trick>(), Team.east_west: List<Trick>() }
    );
  }

  GameState handlePlay(PlayingCard play) {
    List<PlayingCard> playerCopy = List<PlayingCard>.from(players[currentPlayer]);
    playerCopy.remove(play);

    Trick trickCopy = currentTrick.play(currentPlayer, play);

    Map<TablePosition, List<PlayingCard>> newPlayers = players.map((pos, hand) => MapEntry<TablePosition, List<PlayingCard>>(pos, pos == currentPlayer ? playerCopy : hand));

    return GameState(
      phase: trickCopy.finished ? GamePhase.resolve_trick : GamePhase.play,
      dealerPosition: dealerPosition,
      players: newPlayers,
      kitty: kitty,
      currentPlayer: currentPlayer.next,
      bidWinner: bidWinner,
      trump: trump,
      currentTrick: trickCopy,
      pastTricks: pastTricks
    );
  }

  GameState newTrick() {
    if (!currentTrick.finished) {
      throw new Exception("trick is incomplete");
    }
    TablePosition winner = currentTrick.determineWinner();
    Map<Team, List<Trick>> pastTricksCopy = pastTricks.map((k, v) => MapEntry<Team, List<Trick>>(k, List<Trick>.from(v)));
    pastTricksCopy[winner.team].add(currentTrick);
    return GameState(
      phase: GamePhase.play,
      dealerPosition: dealerPosition,
      players: players,
      kitty: kitty,
      currentPlayer: winner,
      bidWinner: bidWinner,
      trump: trump,
      currentTrick: Trick(trump, winner, Map<TablePosition, PlayingCard>()),
      pastTricks: pastTricksCopy
    );
  }

  GameState gameOver() => GameState(
    phase: GamePhase.finished,
    dealerPosition: dealerPosition,
    players: players,
    kitty: kitty,
    currentPlayer: dealerPosition.next,
    bidWinner: bidWinner,
    trump: trump,
    currentTrick: currentTrick,
    pastTricks: pastTricks
  );

  GameState setTrumpAndPlay(Suit trump) => GameState(
    phase: GamePhase.play,
    dealerPosition: dealerPosition,
    players: players,
    kitty: kitty,
    currentPlayer: dealerPosition.next,
    bidWinner: currentPlayer,
    trump: trump,
    currentTrick: Trick(trump, dealerPosition.next, Map<TablePosition, PlayingCard>()),
    pastTricks: pastTricks
  );

  int get totalTricks => pastTricks[Team.north_south].length + pastTricks[Team.east_west].length;
}