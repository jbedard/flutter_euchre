import 'package:app/cards/card-scoring.dart';
import 'package:app/cards/playing-card.dart';
import 'package:app/cards/suit.dart';
import 'package:app/game/table-position.dart';

class Trick {
  final Suit trump;
  final TablePosition lead;
  final Map<TablePosition, PlayingCard> plays;

  Trick(this.trump, this.lead, this.plays);

  TablePosition determineWinner() {
    TablePosition pos = lead;
    Suit leadSuit = this.leadSuit;
    int highScore = trickCardScore(plays[pos], leadSuit, trump);
    TablePosition winner = pos;
    for (int i = 0; i < 3; i++) {
      pos = pos.next;
      if (!plays.containsKey(pos)) {
        return winner;
      }
      int s = trickCardScore(plays[pos], leadSuit, trump);
      if (s > highScore) {
        highScore = s;
        winner = pos;
      }
    }
    return winner;
  }

  bool get empty => this.plays.isEmpty;

  bool get finished => this.plays.length == 4;

  Suit get leadSuit => this.plays[lead].getSuit(trump);

  Trick copy() => Trick(trump, lead, Map.from(plays));

  Trick play(TablePosition pos, PlayingCard card) {
    Map<TablePosition, PlayingCard> newPlays = Map.from(plays);
    if (newPlays.containsKey(pos)) {
      throw new Exception("invalid. A play already exists for " + pos.name);
    }
    newPlays[pos] = card;
    return Trick(trump, lead, newPlays);
  }
}