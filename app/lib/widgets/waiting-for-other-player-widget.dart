import 'package:app/game/table-position.dart';
import 'package:flutter/cupertino.dart';

class WaitingForOtherPlayerWidget extends StatelessWidget {
  final TablePosition position;

  const WaitingForOtherPlayerWidget(this.position);

  @override
  Widget build(BuildContext context) {
    return Column(children: <Widget>[Text("waiting for"), Text(position.name)],);
  }
}