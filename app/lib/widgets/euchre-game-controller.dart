import 'dart:async';

import 'package:app/ai/derp-ai.dart';
import 'package:app/ai/euchre-ai.dart';
import 'package:app/ai/good-ai.dart';
import 'package:app/ai/perfect-cheating-ai.dart';
import 'package:app/cards/playing-card.dart';
import 'package:app/cards/suit.dart';
import 'package:app/game/call-result.dart';
import 'package:app/game/game-phase.dart';
import 'package:app/game/game-state.dart';
import 'package:app/game/table-position.dart';
import 'package:app/game/team.dart';
import 'package:app/widgets/phases/bid-first-round-widget.dart';
import 'package:app/widgets/phases/bid-second-round-widget.dart';
import 'package:app/widgets/phases/discard-phase-widget.dart';
import 'package:app/widgets/phases/game-over-widget.dart';
import 'package:app/widgets/phases/play-phase-widget.dart';
import 'package:flutter/material.dart';

import '../game/game-phase.dart';

class EuchreGameController extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => EuchreGameControllerState();
}

class EuchreGameControllerState extends State<EuchreGameController> {
  GameState _euchreGameState;
  EuchreAI _ai;

  int _northSouthPoints = 0;
  int _eastWestPoints = 0;

  @override
  void initState() {
    super.initState();

    _euchreGameState = startNewGame(TablePosition.east);

    _ai = PerfectCheatingAI();
  }

  void _maybeProcessAITurn() {
    if (_euchreGameState.currentPlayer != TablePosition.south) {
      switch (_euchreGameState.phase) {
        case GamePhase.first_bidding_round:
          _ai.desiresBid(_euchreGameState).then(_onFirstRoundBidResult);
          break;
        case GamePhase.second_bidding_round:
          _ai.maybeCallSuit(_euchreGameState).then(_onSecondRoundBidResult);
          break;
        case GamePhase.discard:
          _ai.chooseDiscard(_euchreGameState).then(_onDiscard);
          break;
        case GamePhase.play:
          _ai.play(_euchreGameState).then(_onPlay);
          break;
        default:
          // ai does nothing for other phases
          break;
      }
    }
  }

  Widget _simpleTable(Size size, Widget child) => Container(
    color: Colors.green,
    child: Center(
      child: Container(
        width: size.width,
        height: size.height,
        child: child
      ),
    ),
  );

  Widget _tableWithTrumpAndScore(Size size, Widget child) {
    OrientationBuilder theGame = OrientationBuilder(
      builder: (context, orientation) {
        List<Widget> items = [
          Column(children: [
            Text("team ${_euchreGameState.bidWinner.team.name} won the bid."),
            Text("trump is ${_euchreGameState.trump.shortName}"),
          ]),
          Container(
            width: size.width,
            height: size.height,
            child: child
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Column(children: [
                Text("tricks"),
                Text("N/S ${_euchreGameState.pastTricks[Team.north_south].length}"),
                Text("E/W ${_euchreGameState.pastTricks[Team.east_west].length}")
              ]),
              Column(children: [
                Text("points"),
                Text("N/S ${_northSouthPoints}"),
                Text("E/W ${_eastWestPoints}")
              ])
            ]
          ),
        ];
        return orientation == Orientation.portrait
          ? Column(
            key: UniqueKey(),
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: items
          )
          : Column(
            key: UniqueKey(),
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: items
              )
            ]
          );
      },
    );
    return Container(
      color: Colors.green,
      child: theGame
    );
  }

  void _onFirstRoundBidResult(bool pickUp) {
    setState(() {
      if (pickUp) {
        _euchreGameState = _euchreGameState.orderUp();
      } else {
        if (_euchreGameState.currentPlayer == _euchreGameState.dealerPosition) {
          _euchreGameState = _euchreGameState.withPhase(GamePhase.second_bidding_round);
        } else {
          _euchreGameState = _euchreGameState.nextPlayer();
        }
      }
      _maybeProcessAITurn();
    });
  }

  void _onSecondRoundBidResult(CallResult result) {
    setState(() {
      if (result.call) {
        _euchreGameState = _euchreGameState.setTrumpAndPlay(result.suit);
      } else {
        if (_euchreGameState.currentPlayer == _euchreGameState.dealerPosition) {
            _euchreGameState = startNewGame(_euchreGameState.dealerPosition.next);
        } else {
            _euchreGameState = _euchreGameState.nextPlayer();
        }
      }
      _maybeProcessAITurn();
    });
  }

  void _onDiscard(PlayingCard card) {
    setState(() {
      _euchreGameState = _euchreGameState.handleDiscard(card);
      _maybeProcessAITurn();
    });
  }

  void _onPlay(PlayingCard card) {
    setState(() {
      _euchreGameState = _euchreGameState.handlePlay(card);
      if (_euchreGameState.phase == GamePhase.resolve_trick) {
        Timer(Duration(milliseconds: 800), () {
          setState(() {
            _euchreGameState = _euchreGameState.newTrick();
            if (_euchreGameState.totalTricks == 5) {
              _euchreGameState = _euchreGameState.gameOver();
              _handleScoreUpdates();
            } else {
              _maybeProcessAITurn();
            }
          });
        });
      } else {
        _maybeProcessAITurn();
      }
    });
  }

  void _handleScoreUpdates() {
    if (_euchreGameState.totalTricks != 5) {
      throw Exception("game's not over!");
    }
    if (_euchreGameState.pastTricks[Team.north_south].length > _euchreGameState.pastTricks[Team.east_west].length) {
      _northSouthPoints += (_euchreGameState.bidWinner.team == Team.north_south) ? 1 : 2;
    } else {
      _eastWestPoints += (_euchreGameState.bidWinner.team == Team.east_west) ? 1 : 2;
    }
  }

  @override
  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;
    Size tableSize = screenSize.width > screenSize.height
      ? Size(screenSize.height * .9, screenSize.height * .9)
      : Size(screenSize.width * .9, screenSize.width * .9);
    double cardHeight = tableSize.height / 6.0;
    double cardWidth = cardHeight * .714;
    Size cardSize = Size(cardWidth, cardHeight);
    
    switch (_euchreGameState.phase) {
      case GamePhase.first_bidding_round:
        return _simpleTable(tableSize, BidFirstRoundWidget(
          cardSize: cardSize,
          state: _euchreGameState,
          onResult: _onFirstRoundBidResult
        ));
      case GamePhase.second_bidding_round:
        return _simpleTable(tableSize, BidSecondRoundWidget(
          cardSize: cardSize,
          state: _euchreGameState,
          onResult: _onSecondRoundBidResult
        ));
      case GamePhase.discard:
        return _simpleTable(tableSize, DiscardPhaseWidget(
          cardSize: cardSize,
          state: _euchreGameState,
          onResult: _onDiscard
        ));
      case GamePhase.play:
        return _tableWithTrumpAndScore(tableSize, PlayPhaseWidget(
          cardSize: cardSize,
          state: _euchreGameState,
          onResult: _onPlay
        ));
      case GamePhase.resolve_trick:
        return _tableWithTrumpAndScore(tableSize, PlayPhaseWidget(
          cardSize: cardSize,
          state: _euchreGameState,
          onResult: null
        ));
      case GamePhase.finished:
        return _simpleTable(tableSize, GameOverWidget(cardSize: cardSize, state: _euchreGameState, onNewGame: () {
          setState(() {
            _euchreGameState = startNewGame(_euchreGameState.dealerPosition.next);
            _maybeProcessAITurn();
          });
        },));
    }
  }
}