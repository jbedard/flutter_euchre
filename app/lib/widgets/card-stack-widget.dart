import 'package:flutter/material.dart';

class CardStackWidget extends StatelessWidget {
  final double overlap;
  final List<Widget> cards;
  final Size cardSize;

  const CardStackWidget(this.overlap, this.cards, this.cardSize);

  Widget _cardStack() {
    List<Widget> children = List<Widget>();
    double pos = 0;
    double overlappedCardSize = cardSize.width * (1.0 - overlap);
    for(Widget w in cards) {
      children.add(Positioned(
        top: 0,
        left: pos,
        child: w
      ));
      pos += overlappedCardSize;
    }
    return Container(
      height: cardSize.height,
      width: cardSize.width + (cards.length - 1) * overlappedCardSize,
      child: Stack(children: children)
    );
  }

  Widget _cards() => cards.length == 1 ? cards[0] : _cardStack();

  @override
  Widget build(BuildContext context) => cards.length == 0 ? SizedBox(height: cardSize.height, width: cardSize.width,) : _cards();
}