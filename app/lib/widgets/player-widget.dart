import 'package:app/cards/card-scoring.dart';
import 'package:app/cards/card-selected-callback.dart';
import 'package:app/cards/card-util.dart';
import 'package:app/cards/playing-card.dart';
import 'package:app/game/game-phase.dart';
import 'package:app/game/game-state.dart';
import 'package:app/game/kitty.dart';
import 'package:app/game/table-position.dart';
import 'package:app/widgets/simple-card-widget.dart';
import 'package:flutter/cupertino.dart';

import 'card-stack-widget.dart';
import 'kitty-widget.dart';

double defaultOverlapSize = .7;
double defaultSpacingSize = -.1;

class _CardStack extends StatelessWidget {
  final List<Widget> cards;
  final Size cardSize;
  final double overlap;

  const _CardStack({this.cards, this.cardSize, this.overlap});

  @override
  Widget build(BuildContext context) => CardStackWidget(overlap, cards, cardSize);
}

class _KittyWithCards extends StatelessWidget {
  final Widget cards;
  final Kitty kitty;
  final Size cardSize;

  const _KittyWithCards({this.cards, this.kitty, this.cardSize});

  @override
  Widget build(BuildContext context) => Row(
    mainAxisAlignment: MainAxisAlignment.center,
    children: <Widget>[
      cards,
      SizedBox(width: cardSize.width * .2, height: cardSize.height,),
      KittyWidget(cardSize: cardSize, kitty: kitty)
    ]
  );
}

class PlayerWidget extends StatelessWidget {
  final GameState state;
  final Size cardSize;
  final CardSelectedCallback onSelected;

  const PlayerWidget({this.state, this.cardSize, this.onSelected});

  Widget _nonInteractive(List<PlayingCard> cards) => _CardStack(
    cards: cards.map((c) => SimpleCardWidget(c, cardSize)).toList(),
    cardSize: cardSize,
    overlap: defaultSpacingSize,
  );

  @override
  Widget build(BuildContext context) {
    bool playerTurn = state.currentPlayer == TablePosition.south;
    List<PlayingCard> cards = state.players[TablePosition.south];

    switch (state.phase) {
      case GamePhase.first_bidding_round:
      case GamePhase.discard:
        cards.sort((c1, c2) => cardScoreWithTrump(c1, state.kitty.upCard.baseSuit) - cardScoreWithTrump(c2, state.kitty.upCard.baseSuit));
        break;
      case GamePhase.second_bidding_round:
        cards.sort((c1, c2) => absoluteCardScore(c1) - absoluteCardScore(c2));
        break;
      default:
        cards.sort((c1, c2) => cardScoreWithTrump(c1, state.trump) - cardScoreWithTrump(c2, state.trump));
        break;
    }
    
    switch(state.phase) {
      case GamePhase.first_bidding_round:
        return state.dealerPosition == TablePosition.south ? _KittyWithCards(cards: _nonInteractive(cards), cardSize: cardSize, kitty: state.kitty,) : _nonInteractive(cards);
      case GamePhase.second_bidding_round:
      case GamePhase.resolve_trick:
      case GamePhase.finished:
        return _nonInteractive(cards);
      case GamePhase.discard:
        return playerTurn ? _CardStack(
          cards: cards.map((c) => TappableCardWidget(c, cardSize, () { this.onSelected(c); })).toList(),
          cardSize: cardSize,
          overlap: defaultSpacingSize,
        ) : _nonInteractive(cards);
      case GamePhase.play:
        if (playerTurn) {
          List<PlayingCard> valid = getValidPlays(cards, state.currentTrick, state.trump);
          return _CardStack(
            cards: cards.map((c) => valid.contains(c) ? TappableCardWidget(c, cardSize, () { this.onSelected(c); }) : SimpleCardWidget(c, cardSize)).toList(),
            cardSize: cardSize,
            overlap: defaultSpacingSize,
          );
        } else {
          return _nonInteractive(cards);
        }
    }
  }
}

class OpponentWidget extends StatelessWidget {
  final TablePosition position;
  final GameState state;
  final Size cardSize;

  const OpponentWidget({this.position, this.state, this.cardSize});

  @override
  Widget build(BuildContext context) {
    List<PlayingCard> cards = state.players[position];
    switch(state.phase) {
      case GamePhase.first_bidding_round:
      case GamePhase.discard:
        //List<Widget> cardBacks = cards.map((c) => CardBackWidget(cardSize)).toList();
        List<Widget> cardBacks = cards.map((c) => SimpleCardWidget(c, cardSize)).toList();
        _CardStack stack = _CardStack(cards: cardBacks, cardSize: cardSize, overlap: .3);
        return position == state.dealerPosition ? _KittyWithCards(cards: stack, cardSize: cardSize, kitty: state.kitty) : stack;
      case GamePhase.second_bidding_round:
      case GamePhase.play:
      case GamePhase.resolve_trick:
      case GamePhase.finished:
        //return _CardStack(cards: cards.map((c) => CardBackWidget(cardSize)).toList(), cardSize: cardSize, overlap: defaultOverlapSize);
        return _CardStack(cards: cards.map((c) => SimpleCardWidget(c, cardSize)).toList(), cardSize: cardSize, overlap: .2);
    }
  }
}