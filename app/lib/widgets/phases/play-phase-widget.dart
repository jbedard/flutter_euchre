import 'package:app/cards/card-selected-callback.dart';
import 'package:app/game/game-state.dart';
import 'package:app/game/table-position.dart';
import 'package:app/widgets/euchre-table-widget.dart';
import 'package:app/widgets/player-widget.dart';
import 'package:app/widgets/trick-widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class PlayPhaseWidget extends StatelessWidget {
  final Size cardSize;
  final GameState state;
  final CardSelectedCallback onResult;

  const PlayPhaseWidget({this.cardSize, this.state, this.onResult});

  @override
  Widget build(BuildContext context) => EuchreTableWidget(
    players: state.players.map((k, v) => MapEntry<TablePosition, Widget>(k, k == TablePosition.south ?
      PlayerWidget(state: state, cardSize: cardSize, onSelected: onResult,)
      : OpponentWidget(position: k, state: state, cardSize: cardSize))),
    tableCenter: TrickWidget(state.currentTrick.plays, cardSize)
  );
}