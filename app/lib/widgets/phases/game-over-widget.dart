import 'package:app/game/game-state.dart';
import 'package:app/game/team.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class GameOverWidget extends StatelessWidget {
  final Size cardSize;
  final GameState state;
  final VoidCallback onNewGame;

  const GameOverWidget({this.cardSize, this.state, this.onNewGame});

  Widget _centerWidget() {
    Team winner = state.pastTricks[Team.north_south].length > state.pastTricks[Team.east_west].length ? Team.north_south : Team.east_west;
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Text("${winner.name} has won the game with ${state.pastTricks[winner].length} tricks!"),
        RaisedButton(child: Text("play again"), onPressed: onNewGame,)
      ]
    );
  }

  @override
  Widget build(BuildContext context) => Center(child: _centerWidget());
}