import 'package:app/cards/card-selected-callback.dart';
import 'package:app/game/game-state.dart';
import 'package:app/game/table-position.dart';
import 'package:app/widgets/euchre-table-widget.dart';
import 'package:app/widgets/player-widget.dart';
import 'package:app/widgets/waiting-for-other-player-widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class DiscardPhaseWidget extends StatelessWidget {
  final Size cardSize;
  final GameState state;
  final CardSelectedCallback onResult;

  const DiscardPhaseWidget({this.cardSize, this.state, this.onResult});
  
  @override
  Widget build(BuildContext context) => EuchreTableWidget(
    players: state.players.map((k, v) => MapEntry<TablePosition, Widget>(k, k == TablePosition.south ?
      PlayerWidget(state: state, cardSize: cardSize, onSelected: onResult,)
      : OpponentWidget(position: k, state: state, cardSize: cardSize))),
    tableCenter: state.currentPlayer == TablePosition.south ? Text("discard a card") : WaitingForOtherPlayerWidget(state.currentPlayer)
  );
}