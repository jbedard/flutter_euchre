
import 'package:app/game/game-state.dart';
import 'package:app/game/table-position.dart';
import 'package:app/widgets/euchre-table-widget.dart';
import 'package:app/widgets/player-widget.dart';
import 'package:app/widgets/waiting-for-other-player-widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

typedef BidCallback = void Function(bool pickUp);

class BidFirstRoundWidget extends StatelessWidget {
  final Size cardSize;
  final GameState state;
  final BidCallback onResult;

  const BidFirstRoundWidget({this.cardSize, this.state, this.onResult});

  Widget _buttons() => Column(children: <Widget>[
    RaisedButton(onPressed: () {
      this.onResult(true);
    }, child: Text("pick up"),),
    RaisedButton(onPressed: () {
      this.onResult(false);
    }, child: Text("pass"),),
  ],);

  @override
  Widget build(BuildContext context) => EuchreTableWidget(
    players: state.players.map((k, v) => MapEntry<TablePosition, Widget>(k, k == TablePosition.south ?
      PlayerWidget(state: state, cardSize: cardSize, onSelected: null,)
      : OpponentWidget(position: k, state: state, cardSize: cardSize))),
    tableCenter: state.currentPlayer == TablePosition.south ? _buttons() : WaitingForOtherPlayerWidget(state.currentPlayer)
  );
}