import 'package:app/ai/euchre-ai.dart';
import 'package:app/cards/suit.dart';
import 'package:app/game/call-result.dart';
import 'package:app/game/game-state.dart';
import 'package:app/game/table-position.dart';
import 'package:app/widgets/euchre-table-widget.dart';
import 'package:app/widgets/player-widget.dart';
import 'package:app/widgets/waiting-for-other-player-widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class BidSecondRoundWidget extends StatelessWidget {
  final Size cardSize;
  final GameState state;
  final CallSuitCallback onResult;

  const BidSecondRoundWidget({this.cardSize, this.state, this.onResult});

  Widget _suitButton(Suit suit) => RaisedButton(
    child: Text(suit.shortName),
    onPressed: () {
      this.onResult(CallResult(true, suit));
    }
  );

  Widget _callSuit() => Column(
    children: <Widget>[
      Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          _suitButton(Suit.spades), SizedBox(width: 16), _suitButton(Suit.hearts)
        ],
      ),
      Row(
        children: <Widget>[
          _suitButton(Suit.diamonds), SizedBox(width: 16), _suitButton(Suit.clubs)
        ],
      ),
      Row(
        children: <Widget>[
          RaisedButton(child: Text("pass"), onPressed: () { this.onResult(CallResult(false, Suit.spades)); },)
        ],
      )
    ],
  );

  @override
  Widget build(BuildContext context) => EuchreTableWidget(
    players: state.players.map((k, v) => MapEntry<TablePosition, Widget>(k, k == TablePosition.south ?
      PlayerWidget(state: state, cardSize: cardSize, onSelected: null,)
      : OpponentWidget(position: k, state: state, cardSize: cardSize))),
    tableCenter: state.currentPlayer == TablePosition.south ? _callSuit() : WaitingForOtherPlayerWidget(state.currentPlayer),
  );
}