import 'package:app/game/table-position.dart';
import 'package:flutter/material.dart';

class EuchreTableWidget extends StatelessWidget {
  final Map<TablePosition, Widget> players;
  final Widget tableCenter;

  const EuchreTableWidget({this.players, this.tableCenter});

  @override
  Widget build(BuildContext context) => Column(
    mainAxisAlignment: MainAxisAlignment.spaceBetween,
    children: <Widget>[
      RotatedBox(quarterTurns: 2, child: players[TablePosition.north]),
      Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          RotatedBox(quarterTurns: 1, child: players[TablePosition.west]),
          tableCenter,
          RotatedBox(quarterTurns: 3, child: players[TablePosition.east]),
        ]
      ),
      players[TablePosition.south],
    ]
  );
}