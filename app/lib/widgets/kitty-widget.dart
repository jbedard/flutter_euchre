import 'package:app/game/kitty.dart';
import 'package:app/widgets/simple-card-widget.dart';
import 'package:flutter/cupertino.dart';

import 'card-stack-widget.dart';

class KittyWidget extends StatelessWidget {
  final Size cardSize;
  final Kitty kitty;

  const KittyWidget({this.cardSize, this.kitty});
  
  @override
  Widget build(BuildContext context) => CardStackWidget(.9, [
    CardBackWidget(cardSize),
    CardBackWidget(cardSize),
    CardBackWidget(cardSize),
    SimpleCardWidget(kitty.upCard, cardSize)
  ], cardSize);
}