
import 'package:app/cards/playing-card.dart';
import 'package:flutter/material.dart';

double _cardBorderRadius = 4.0;

class _CardContainerPart extends StatelessWidget {
  final PlayingCard card;
  final Size size;

  _CardContainerPart(this.card, this.size);

  @override
  Widget build(BuildContext context) => Container(
    width: size.width,
    height: size.height,
    child: Center(
      child: Text(card.toString(), style: TextStyle(fontSize: size.width > 60 ? 20 : 12),)
    )
  );
}

class _WhitePart extends StatelessWidget {
  final Size size;
  final Widget child;

  const _WhitePart({this.size, this.child});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(_cardBorderRadius),
        color: Colors.white
      ),
      child: child,
    );
  }
}

class SimpleCardWidget extends StatelessWidget {
  final PlayingCard card;
  final Size size;

  SimpleCardWidget(this.card, this.size);

  @override
  Widget build(BuildContext context) => _WhitePart(child: _CardContainerPart(this.card, this.size));
}

class TappableCardWidget extends StatelessWidget {
  final PlayingCard card;
  final Size size;
  final GestureTapCallback onTap;

  TappableCardWidget(this.card, this.size, this.onTap);

  @override
  Widget build(BuildContext context) => _WhitePart(
    child: new InkWell(
    onTap: onTap,
    child: _CardContainerPart(this.card, this.size)
  ));
}

class CardBackWidget extends StatelessWidget {
  final Size size;

  CardBackWidget(this.size);

  @override
  Widget build(BuildContext context) => Container(
    width: size.width,
    height: size.height,
    decoration: BoxDecoration(
      borderRadius: BorderRadius.circular(_cardBorderRadius),
      color: Colors.white
    ),
    child: Padding(
      padding: EdgeInsets.all(2),
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(_cardBorderRadius),
          color: Colors.blue
        ),
      )
    )
  );
}

class BlankCardWidget extends StatelessWidget {
  final Size size;

  BlankCardWidget(this.size);

  @override
  Widget build(BuildContext context) => Container(
    decoration: BoxDecoration(
      borderRadius: BorderRadius.circular(_cardBorderRadius),
      border: Border.all()
    ),
    width: size.width,
    height: size.height
  );
}