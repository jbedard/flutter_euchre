import 'package:app/cards/playing-card.dart';
import 'package:app/game/table-position.dart';
import 'package:app/widgets/simple-card-widget.dart';
import 'package:flutter/cupertino.dart';

class TrickWidget extends StatelessWidget {
  final Map<TablePosition, PlayingCard> cards;
  final Size cardSize;

  TrickWidget(this.cards, this.cardSize);

  Widget _getCard(TablePosition pos) => cards.containsKey(pos) ? SimpleCardWidget(cards[pos], cardSize) : BlankCardWidget(cardSize);

  @override
  Widget build(BuildContext context) {
    return Column(children: <Widget>[
      _getCard(TablePosition.north),
      Row(children: <Widget>[
        RotatedBox(quarterTurns: 1, child: _getCard(TablePosition.west)),
        SizedBox(height: cardSize.width, width: cardSize.width,),
        RotatedBox(quarterTurns: 3, child: _getCard(TablePosition.east)),
      ],),
      _getCard(TablePosition.south),
    ],);
  }
}