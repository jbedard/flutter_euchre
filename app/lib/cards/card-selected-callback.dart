import 'package:app/cards/playing-card.dart';

typedef CardSelectedCallback = void Function(PlayingCard card);