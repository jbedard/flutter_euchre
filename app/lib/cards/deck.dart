import 'package:app/cards/playing-card.dart';
import 'package:app/cards/suit.dart';

import 'card-value.dart';


List<PlayingCard> newShuffledDeck() {
  List<PlayingCard> cards = List<PlayingCard>();
  for (Suit s in Suit.values) {
    for (CardValue v in CardValue.values) {
      cards.add(PlayingCard(s, v));
    }
  }
  cards.shuffle();
  return cards;
}