import 'package:app/cards/playing-card.dart';
import 'package:app/cards/suit.dart';

import 'card-value.dart';

int absoluteCardScore(PlayingCard card) {
  int suitVal = 0;
  switch (card.baseSuit) {
    case Suit.spades:
      suitVal = 18;
      break;
    case Suit.hearts:
      suitVal = 12;
      break;
    case Suit.diamonds:
      suitVal = 6;
      break;
    case Suit.clubs:
      break;
  }
  return suitVal + card.value.baseValue;
}

int cardScoreWithTrump(PlayingCard card, Suit trump) {
  Suit suit = card.getSuit(trump);
  if (suit == trump) {
    switch (card.value) {
      case CardValue.jack:
      return card.baseSuit == trump ? 24 : 23;
      case CardValue.ace:
      return 22;
      case CardValue.king:
      return 21;
      case CardValue.queen:
      return 20;
      case CardValue.ten:
      return 19;
      case CardValue.nine:
      return 18;
    }
  } else if (suit == trump.opposite) {
    switch (card.value) {
      case CardValue.ace:
      return 17;
      case CardValue.king:
      return 16;
      case CardValue.queen:
      return 15;
      case CardValue.jack:
      return 14;
      case CardValue.ten:
      return 13;
      case CardValue.nine:
      return 12;
    }
  } else if (suit == trump.sister) {
    switch (card.value) {
      case CardValue.ace:
      return 11;
      case CardValue.king:
      return 10;
      case CardValue.queen:
      return 9;
      case CardValue.jack:
      throw new Exception("sister jack is the left bauer");
      case CardValue.ten:
      return 8;
      case CardValue.nine:
      return 7;
    }
  } else if (suit == trump.opposite.sister) {
    switch (card.value) {
      case CardValue.ace:
      return 6;
      case CardValue.king:
      return 5;
      case CardValue.queen:
      return 4;
      case CardValue.jack:
      return 3;
      case CardValue.ten:
      return 2;
      case CardValue.nine:
      return 1;
    }
  }
  throw new Exception("all cards are covered");
}

int trickCardScore(PlayingCard card, Suit lead, Suit trump) {
  bool isTrump = card.getSuit(trump) == trump;
  if (isTrump) {
    switch (card.value) {
      case CardValue.jack:
      return card.baseSuit == trump ? 13 : 12;
      case CardValue.ace:
      return 11;
      case CardValue.king:
      return 10;
      case CardValue.queen:
      return 9;
      case CardValue.ten:
      return 8;
      case CardValue.nine:
      return 7;
    }
  } else if (card.baseSuit == lead) {
    switch (card.value) {
      case CardValue.ace:
      return 6;
      case CardValue.king:
      return 5;
      case CardValue.queen:
      return 4;
      case CardValue.jack:
      return 3;
      case CardValue.ten:
      return 2;
      case CardValue.nine:
      return 1;
    }
  }
  return 0;
}

int discardScore(PlayingCard card, Suit trump) {
  Suit suit = card.getSuit(trump);
  if (suit == trump) {
    switch (card.value) {
      case CardValue.jack:
      return card.baseSuit == trump ? 12 : 11;
      case CardValue.ace:
      return 10;
      case CardValue.king:
      return 9;
      case CardValue.queen:
      return 8;
      case CardValue.ten:
      return 7;
      case CardValue.nine:
      return 6;
    }
  }
  return card.value.baseValue;
}