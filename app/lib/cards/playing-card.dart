import 'package:app/cards/suit.dart';

import 'card-value.dart';

class PlayingCard {
  final Suit _suit;
  final CardValue value;

  PlayingCard(this._suit, this.value);

  String toString() {
    return this.value.shortName + this._suit.shortName;
  }

  Suit get baseSuit => _suit;

  Suit getSuit(Suit trump) {
    return value == CardValue.jack && _suit == trump.sister ? trump : _suit;
  }
}