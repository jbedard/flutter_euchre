enum CardValue {
  nine,
  ten,
  jack,
  queen,
  king,
  ace
}

extension CardValueExtension on CardValue {
  String get shortName {
    switch (this) {
      case CardValue.nine:
        return "9";
      case CardValue.ten:
        return "10";
      case CardValue.jack:
        return "J";
      case CardValue.queen:
        return "Q";
      case CardValue.king:
        return "K";
      case CardValue.ace:
        return "A";
    }
  }

  int get baseValue {
    switch (this) {
      case CardValue.nine:
        return 0;
      case CardValue.ten:
        return 1;
      case CardValue.jack:
        return 2;
      case CardValue.queen:
        return 3;
      case CardValue.king:
        return 4;
      case CardValue.ace:
        return 5;
    }
  }
}