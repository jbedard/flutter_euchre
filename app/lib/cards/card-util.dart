import 'package:app/game/trick.dart';

import 'playing-card.dart';
import 'suit.dart';

List<PlayingCard> getValidPlays(List<PlayingCard> cards, Trick trick, Suit trump) {
  if (trick.empty) {
    return List<PlayingCard>.from(cards);
  }
  List<PlayingCard> cardsMatchingLead = cards.where((c) => c.getSuit(trump) == trick.leadSuit).toList();
  return cardsMatchingLead.isEmpty ? List<PlayingCard>.from(cards) : cardsMatchingLead;
}