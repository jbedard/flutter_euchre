enum Suit {
  spades,
  hearts,
  diamonds,
  clubs
}

extension SuitExtension on Suit {
  String get shortName {
    switch (this) {
      case Suit.spades:
        return "♠";
      case Suit.hearts:
        return "♥";
      case Suit.diamonds:
        return "♦";
      case Suit.clubs:
        return "♣";
    }
  }

  // suit of same color
  Suit get sister {
    switch (this) {
      case Suit.spades:
        return Suit.clubs;
      case Suit.hearts:
        return Suit.diamonds;
      case Suit.diamonds:
        return Suit.hearts;
      case Suit.clubs:
        return Suit.spades;
    }
  }

  // different color suit. spades<->diamonds, hearts<->clubs.
  Suit get opposite {
    switch (this) {
      case Suit.spades:
        return Suit.diamonds;
      case Suit.hearts:
        return Suit.clubs;
      case Suit.diamonds:
        return Suit.spades;
      case Suit.clubs:
        return Suit.hearts;
    }
  }
}