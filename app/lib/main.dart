import 'package:app/widgets/euchre-game-controller.dart';
import 'package:flutter/material.dart';

void main() => runApp(EuchreApp());

class EuchreApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Euchre AI',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: EuchreGame(),
    );
  }
}

class EuchreGame extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: EuchreGameController()
    );
  }
}